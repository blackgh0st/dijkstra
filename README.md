# Dijkstra's Single Source Shortest Path Algorithm

## Notes

+ Main loop still needs to be completed to run in $`\Theta(n^2)`$ time


## Input Format

+ source node

+ List of weighted edges
    - Edge $`uv`$ with a weight of $`w`$ should be input as $`u\,v\,w`$



## Compile and Run

+ `make` to compile

+ `make run` to run

